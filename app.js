require('dotenv').config();
const config = require("./config.js");

const express = require("express");
const app = express();

const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.database, config.username, config.password, {
  host: config.host,
  dialect: 'mysql',
  dialectModule: require("mysql2"),
});

const bodyParser = require("body-parser");
app.use(bodyParser.json());


require("./connectDB.js")(sequelize)
.then(() => {
  // to retrieve all movies
  app.get("/api/movies", (request, response) => {
    const movieModel = require("./models/movieModel.js")(sequelize);
    movieModel.findAll()
    .then(movies => {
      response.json(movies);  
    })
    .catch(err => {
      response.status(500).send("SERVER ERROR");
      console.log(err);
    });    
  });
  
  // to add a new movie
  app.post("/api/movies", (request, response) => {
    let newMovie = request.body;
    const movieModel = require("./models/movieModel.js")(sequelize);
    movieModel.create(newMovie)
    .then(data => {
      response.json(data);  
    })
    .catch(err => {
      response.status(500).send("SERVER ERROR");
      console.log(err);  
    });     
  });

  // to retrieve a movie with the given ID
  app.get("/api/movies/:movieId", (request, response) => {
    let movieId = request.params.movieId;
    const movieModel = require("./models/movieModel.js")(sequelize);
    movieModel.findAll( { where: { id: movieId } })
    .then(result => { 
      response.json(result[0]); 
      console.log(result);
    })
    .catch(err => {
      response.status(404).send("Invalid movie ID");
      console.log(err);  
    });  
  });

  // to update details of a movie with the given ID
  app.put("/api/movies/:movieId", (request, response) => {
    let movieId = request.params.movieId;
    let updateDetails = request.body;
    const movieModel = require("./models/movieModel.js")(sequelize);
    movieModel.update(updateDetails, { where: { id: movieId } })
    .then(data => {
      response.send("Movie updated.");  
      console.log(data);
    })
    .catch(err => {
      console.log(err);
    });
  });

  // to delete a movie with the given ID
  app.delete("/api/movies/:movieId", (request, response) => {
    let movieId = request.params.movieId;
    const movieModel = require("./models/movieModel.js")(sequelize);
    movieModel.destroy({ where: { id: movieId } })
    .then(data => {
      response.send("Movie deleted.");  
      console.log(data);
    })
    .catch(err => {
      console.log(err);
    });
  });

  // to retrieve all directors
  app.get("/api/directors", (request, response) => {
    const directorModel = require("./models/directorModel.js")(sequelize);
    directorModel.findAll()
    .then(directors => {
      response.json(directors);  
    })
    .catch(err => {
      response.status(500).send("SERVER ERROR");
      console.log(err);
    });    
  });

  // to add a new director
  app.post("/api/directors", (request, response) => {
    let newDirector = request.body;
    const directorModel = require("./models/directorModel.js")(sequelize);
    directorModel.create(newDirector)
    .then(data => {
      response.json(data);  
    })
    .catch(err => {
      response.status(500).send("SERVER ERROR");
      console.log(err);  
    });     
  });

  // to retrieve a director with the given ID
  app.get("/api/directors/:directorId", (request, response) => {
    let directorId = request.params.directorId;
    const directorModel = require("./models/directorModel.js")(sequelize);
    directorModel.findAll( { where: { id: directorId } })
    .then(result => { 
      response.json(result[0]); 
      console.log(result);
    })
    .catch(err => {
      response.status(404).send("Invalid director ID");
      console.log(err);  
    });  
  });

  // to update details of a director with the given ID
  app.put("/api/directors/:directorId", (request, response) => {
    let directorId = request.params.directorId;
    let updateDetails = request.body;
    const directorModel = require("./models/directorModel.js")(sequelize);
    directorModel.update(updateDetails, { where: { id: directorId } })
    .then(data => {
      response.send("Director updated.");  
      console.log(data);
    })
    .catch(err => {
      console.log(err);
    });
  });

// to delete a director with the given ID
  app.delete("/api/directors/:directorId", (request, response) => {
    let directorId = request.params.directorId;
    const directorModel = require("./models/directorModel.js")(sequelize);
    directorModel.destroy({ where: { id: directorId } })
    .then(data => {
      response.send("Director deleted.");  
      console.log(data);
    })
    .catch(err => {
      console.log(err);
    });
  });

  app.listen(config.port, () => {
    console.log(`Server started on port ${config.port} `);  
  });
})
.catch(err => {
  console.log(err);
});