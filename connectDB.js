/**
 * Function parameters:
 * sequelize - an instance of Sequelize, where Sequelize is defined in file ./app.js
 * 
 * This function connects to the database and returns a promise based on that 
 */
module.exports = function(sequelize) {
  return new Promise((resolve, reject) => {
    sequelize.authenticate()
    .then(() => {
      console.log('Connection to the database successful');
      resolve();
    })
    .catch(err => {
      console.log('Unable to connect to the database');
      reject(err);
    });
  });
}










