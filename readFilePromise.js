const fs = require("fs");
/**
 * Function parameters:
 * filePath - the path of the file
 * 
 * This function reads the contents of the file located at the given file path and
 * return a promise based on that
 */
module.exports = function readFilePromise(filePath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, "utf-8", (err, data) => {
      if(err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}
  