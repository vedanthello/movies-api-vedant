require('dotenv').config();
const config = require("./config.js");

const Sequelize = require('sequelize');
const sequelize = new Sequelize(config.database, config.username, config.password, {
  host: config.host,
  dialect: 'mysql',
  dialectModule: require("mysql2"),
});

require("./connectDB.js")(sequelize)
.then(() => {
  // add association
  const movieModel = require("./models/movieModel.js")(sequelize);
  const directorModel = require("./models/directorModel.js")(sequelize);
  directorModel.hasMany(movieModel, {foreignKey: "Director", sourceKey: "Name", onDelete: "SET NULL"});
  
  // seed director model
  directorModel.sync({ force: true })
  .then(() => {
    require("./readFilePromise.js")("./data/movies.json")
    .then(data => {
      let moviesArray = JSON.parse(data);
      // find unique directors in the given movies.json file
      let directorNames = moviesArray.map(movie => movie.Director).filter(director => director !== "NA");
      let uniqueDirectorNames = [...new Set(directorNames)];
      
      let allDirectorsInsertedPromise = [];
      uniqueDirectorNames.forEach((directorName, index) => {
        allDirectorsInsertedPromise[index] = new Promise((resolve, reject) => {
          directorModel.create( {"Name": directorName } )
          .then(data => {
            console.log(`Director with id ${index + 1} inserted`);
            resolve();
          })
          .catch(err => {
            console.log(`Error while inserting director with id ${index + 1}`, err);
            reject();
          });
        });
      });  
      Promise.all(allDirectorsInsertedPromise)
      .then(data => {
        console.log("All directors inserted into the directors table");
        // seed movie model
        movieModel.sync({ force: true })
        .then(() => {
          moviesArray.forEach((movie, index) => {
            for (let field in movie) {
              if (movie[field] === "NA") {
                movie[field] = null;
              }
            }
            movieModel.create(movie)
            .then(data => {
              console.log(`Movie with rank ${index + 1} inserted`);
            })
            .catch(err => {
              console.log(`Error while inserting movie with rank ${index + 1}`, err);
            });
          });
        })
        .catch(err => {
          console.log(err);  
        });
      })
      .catch(err => {
        console.log("Not able to insert all directors into the directors table");
        console.log(err);
      });
    })
    .catch(err => {
      console.log(err);
    });
  })
  .catch(err => {
    console.log(err);
  });
})
.catch(err => {
  console.log(err);
});










