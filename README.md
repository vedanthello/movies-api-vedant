# Task 1
Import data into MySQL using Sequelize ORM. The data being given as a json file.
# Task 2
Create REST API's using ExpressJS
## Description
Repo name format: movies-api-first_name

### Directors:
- Get all directors
- Get the director with given ID
- Add a new director
- Update the director with given ID
- Delete the director with given ID

### Movies:
- Get all movies
- Get the movie with given ID
- Add a new movie
- Update the movie with given ID
- Delete the movie with given ID

### API endpoints:

#### Movies resource:

##### To manage the entire collection of movies resource
- URI : /api/movies
- GET : to retrieve all movies
- POST : to add a new movie

##### To manage a single movie resource
- URI: /api/movies/:movieId
- GET : to retrieve a movie
- PUT : to update details of a movie
- DELETE : to remove a movie

#### Directors resource:

##### To manage the entire collection of directors resource
- URI : /api/directors
- GET : to retrieve all directors
- POST : to add a new director

##### To manage a single director resource
- URI: /api/directors/:directorId
- GET : to retrieve a director
- PUT : to update details of a director
- DELETE : to remove a director

# Bugs
1. Even if an invalid movie ID is supplied to delete a movie, the server sends a message as "Movie deleted."
2. Even if an invalid movie ID is supplied to retrieve a movie, the server responds with HTTP status 200.
3. Even if an invalid director ID is supplied to retrieve a director, the server responds with HTTP status 200.
4. Even if an invalid director ID is supplied to delete a director, the server sends a message as "Director deleted."

# Assumptions
1. No two director personalities have the same name
2. The movie to be added using path as api/movies and HTTP request as POST, should have all details

# Do these before running the server
1. Drop movies table, if such table exists
2. Then, run associateAndSeed.js file to seed the database