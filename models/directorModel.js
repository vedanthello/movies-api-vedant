const { Sequelize, Model } = require('sequelize');

/**
 * Function parameters:
 * sequelize - an instance of Sequelize, where Sequelize is defined in file ../app.js
 * 
 * This function models the directors table and returns the resulting model
 */
module.exports = function (sequelize) {
  class Director extends Model {};
  Director.init(
    {
      "id" :  { 
        type: Sequelize.INTEGER,
        allowNull: false,
        unique: true,
        autoIncrement: true
      }, 
      "Name": {
                type: Sequelize.STRING,
                primaryKey: true 
              }
    },
    { 
      sequelize,
      modelName: 'director' 
    }
  );
  return Director;
}