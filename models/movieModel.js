const { Sequelize, Model } = require('sequelize');

/**
 * Function parameters:
 * sequelize - an instance of Sequelize, where Sequelize is defined in file ../app.js
 * 
 * This function models the movies table and returns the resulting model
 */
module.exports = function (sequelize) {
  class Movie extends Model {};
  Movie.init(
    {
      "Rank": Sequelize.INTEGER, 
      "Title": Sequelize.TEXT,
      "Description": Sequelize.TEXT,
      "Runtime": Sequelize.INTEGER, 
      "Genre": Sequelize.TEXT,
      "Rating": Sequelize.FLOAT, 
      "Metascore": Sequelize.INTEGER, 
      "Votes": Sequelize.INTEGER, 
      "Gross_Earning_in_Mil": Sequelize.FLOAT, 
      "Director": Sequelize.STRING,
      "Actor": Sequelize.TEXT,
      "Year": Sequelize.INTEGER
    },
    { 
      sequelize,
      modelName: 'movie' 
    }
  );
  return Movie;
}